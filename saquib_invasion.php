<?php
	/*
		People 		 Rank		Behaviour	room no  alter
		people 1	 	1		best		1			2
		people 2		2		better		2 			3 	- rage people
		people 3		3		good		3			1   - rage people
	*/

	class Invasion{

		function alterRankRoom($rank_room){
			#prepre rage_array
			$rage_array = array();
			foreach ($rank_room as $rank_key => $room) {
				if($rank_key == $room){
					$rage_array[$rank_key] = count($rank_room) < ($rank_key + 1) ?  1 : $rank_room[$rank_key + 1];
				}
			}

			array_shift($rage_array);
			return array_sum($rage_array);

		}
	}

	#Test case number 1
	// $no_of_people = 2;

	#Test case number 2
	$no_of_people = 3;

	$obj = new Invasion();

	#room rank array
	for($i=1; $i<= $no_of_people; $i++) {
		$rank_room[$i] 	= $i;
	}

	echo $obj->alterRankRoom($rank_room);
?>