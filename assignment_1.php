<?php
    $binary_string = "0001100001111100111111110";
    $string_array  = str_split($binary_string, 6);
    $flag = false;
    
    foreach($string_array as $key => $value){
        $first_match  = strpos($value, '1');
        $second_match = strpos($value, '0');
        if(($first_match === false || $second_match === false) && strlen($value) == 6){
            $flag = true;
            break;
        }
    }
    
    if($flag){
      echo "Sorry, sorry!";  
    }else{
      echo "Good luck!";  
    }
?>
